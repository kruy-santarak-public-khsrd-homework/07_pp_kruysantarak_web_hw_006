let startTime = document.querySelector("#start-time");
let stopTime = document.querySelector("#stop-time");
let btn = document.querySelector("#btn");
let currentDateField = document.querySelector("#current-date");
let duration = document.querySelector("#duration");
let totalPayment = document.querySelector("#payment");
let setLiveStopTime;
let setDuration;
let setTotalPaymentSmallerThan30;
let setTotalPaymentGreaterThan30;

// const MS_PER_MINUTE = 600;  //use for testing
const MS_PER_MINUTE = 60000;

currentDateField.innerHTML = `${new Date().toDateString()} ${new Date().toLocaleTimeString()}`;

//set live current date time
let setCurrentDate = setInterval(() => {
  let date = new Date();
  currentDateField.innerHTML = `${date.toDateString()} ${date.toLocaleTimeString()}`
}, 1000);

//This function is use with setInterval in liveStopTime
let setTimeInterval = (ele) => {
  let liveTime = new Date();
  ele.innerHTML = liveTime.toLocaleTimeString();
}

//This function use to count the duration in minute 
let count = 0;
let durationCount = () => {
  count++;
  duration.innerHTML = count;
}

//This function use to check the duration and calculate the money
let paymentFunction = () => {
  if(count >= 15){
    clearInterval(setTotalPaymentSmallerThan30);
    paymentCalculate();
  } else{
    paymentCalculate();
  }
}

//This function use to calculate money payment
let payment = 0;
let paymentCalculate = () => {
  payment += 500;
  totalPayment.innerText = payment;
}

let changeBtnAction = (btnName, newBtnName, removeBtnClass, addBtnClass, removeBtnIcon, addBtnIcon) => {
  let textOfBtn = document.querySelector("#btn-text");
  let btn = document.querySelector("#btn");
  let iconOfBtn = document.querySelector("#icon-in-btn");
  let currentStartTime = new Date();
  stopTime.innerText = currentStartTime.toLocaleTimeString();
  textOfBtn.textContent = newBtnName;
  btn.classList.remove(removeBtnClass);
  btn.classList.add(addBtnClass);
  iconOfBtn.classList.remove(removeBtnIcon);
  iconOfBtn.classList.add(addBtnIcon);
  if (btnName === "Start") {
    payment = 500;
    // startTime.innerHTML = currentStartTime.toLocaleTimeString([], {timeStyle : "short"});
    startTime.innerHTML = currentStartTime.toLocaleTimeString();
    setLiveStopTime = setInterval(setTimeInterval.bind(this, stopTime), 1000);
    setDuration = setInterval(durationCount, MS_PER_MINUTE);
    setTotalPaymentSmallerThan30 = setInterval(paymentFunction, MS_PER_MINUTE * 15);
    setTotalPaymentGreaterThan30 = setInterval(paymentFunction, MS_PER_MINUTE * 30);
    totalPayment.innerText = payment;
  }
  if (btnName === "Stop") {
    clearInterval(setLiveStopTime);
    clearInterval(setDuration);
    clearInterval(setTotalPaymentSmallerThan30);
    clearInterval(setTotalPaymentGreaterThan30)
  }
}

let btnOnclick = () => {
  let textOfBtn = document.querySelector("#btn-text");
  if (textOfBtn.textContent === "Start") {
    changeBtnAction("Start", "Stop", "btn-success", "btn-danger", "fa-play-circle", "fa-stop-circle")
  } else if (textOfBtn.textContent === "Stop") {
    changeBtnAction("Stop", "Clear", "btn-danger", "btn-warning", "fa-stop-circle", "fa-trash-alt")
  } else if (textOfBtn.textContent === "Clear") {
    window.location.reload();
  } else {
    return;
  }

}

btn.addEventListener("click", btnOnclick)

